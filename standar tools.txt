Burp Suite: Alat yang kuat untuk menguji keamanan aplikasi web, termasuk fitur Proxy, Scanner, dan Intruder.
Nmap: Scanner jaringan yang digunakan untuk mengidentifikasi host dan layanan yang berjalan.
SQLMap: Alat otomatis untuk mengeksploitasi kerentanan SQL injection.
OWASP ZAP: Alat pemindaian keamanan aplikasi web open source.
DirBuster: Alat untuk mencari direktori dan berkas tersembunyi di situs web.
Sublist3r: Alat untuk mencari subdomain yang terkait dengan sebuah domain.
Nikto: Scanner web yang mengidentifikasi kerentanan umum pada server web