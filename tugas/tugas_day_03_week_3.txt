Injection Attacks (SQL Injection):
- Pada halaman pencarian produk, input URL atau formulir yang dimaksudkan untuk mencari produk dengan ID tertentu.
- Contoh URL: https://www.example.com/search?productID=1' OR '1'='1'; --


Cross-Site Scripting (XSS):
- Coba sisipkan skrip berbahaya melalui kolom komentar atau formulir kontak.
- Contoh komentar: <script>alert("XSS Attack");</script>


Insecure Direct Object References:
- Cobalah mengganti nilai ID atau URL untuk merujuk ke objek yang seharusnya tidak dapat diakses.
- Contoh URL: https://www.example.com/viewfile?fileID=123


Insecure File Upload:
- Jika situs memungkinkan pengguna mengunggah file, coba kirimkan file berbahaya atau file dengan ekstensi yang tidak seharusnya diizinkan.
_ Misalnya, mengunggah file dengan nama malicious_script.php yang dapat dieksekusi di server.